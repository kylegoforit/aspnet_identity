﻿import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { Link } from '@material-ui/core';

const styles = theme => ({
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        margin: "10px auto"
    },
    button: {
        margin: "10px auto"
    }
});

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            account: {
                email: '',
                password: '',
                rememberMe: ''
            },
            submitted: false
        }
    }

    handleSubmit = () => {

    }

    render() {
        const { classes } = this.props;
        const { account } = this.state;
        const { submitted } = this.state;

        return (
            <div>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={3}
                >
                    <ValidatorForm
                        ref="form"
                        onSubmit={this.handleSubmit}
                    >
                        <Grid item xs={12}>
                            <Typography variant="h4">
                                Login
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextValidator
                                fullWidth
                                variant="outlined"
                                label="User Name"
                                onChange={this.handleChange}
                                name="userName"
                                value={account.email}
                                validators={['required', 'isEmail']}
                                errorMessages={['this field is required', 'email is not valid']}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextValidator
                                fullWidth
                                type="password"
                                variant="outlined"
                                label="Password"
                                onChange={this.handleChange}
                                name="password"
                                value={account.password}
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={account.remember}
                                        onChange={this.handleChange}
                                        value="remember"
                                        color="primary"
                                    />
                                }
                                label="Remember Me"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <div className={classes.button}>
                                <Button
                                    color="primary"
                                    variant="contained"
                                    type="submit"
                                    disabled={submitted}
                                >
                                    {
                                        (submitted && 'Loading...')
                                        || (!submitted && 'Login')
                                    }
                                </Button>
                            </div>
                        </Grid>
                        <Grid item xs={12}>
                            <div className="text-align-right">
                                <Link
                                    component={RouterLink}
                                    to="/account/sign-up"
                                >
                                    Create a new account
                                </Link>
                            </div>
                        </Grid>
                    </ValidatorForm>
                </Grid>
            </div>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);