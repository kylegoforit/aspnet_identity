﻿import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import FormHelperText from '@material-ui/core/FormHelperText';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { Link } from '@material-ui/core';

const styles = theme => ({
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        margin: "10px auto"
    },
    button: {
        margin: "10px auto"
    },
    error: {
        fontSize: 11
    }
});

class SignUp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            account: {
                email: '',
                password: '',
                confirmPassword: ''
            },
            response: {
                succeeded: true,
                code: '',
                message: ''
            },
            submitted: false
        }
    }

    componentDidMount() {
        ValidatorForm.addValidationRule('checkComplexPassword', (value) => {
            var hasUpperCase = /[A-Z]/.test(value);
            var hasLowerCase = /[a-z]/.test(value);
            var hasNumbers = /\d/.test(value);
            var hasNonalphas = /\W/.test(value);
            return (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas) > 3;
        });

        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            if (value !== this.state.account.password) {
                return false;
            }
            return true;
        });
    }

    handleChange = (event) => {
        const { account } = this.state;
        account[event.target.name] = event.target.value;
        this.setState({ account });
        this.setState({
            response: {
                succeeded: true,
                code: '',
                message: ''
            }
        });
    }

    handleSubmit = () => {
        this.setState({ submitted: true }, () => {
            fetch('Account/Register', {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.state.account)
            })
                .then(res => res.json())
                .then(res => {
                    this.props.history.push({
                        pathname: "/account/re-send-verification-link",
                        state: { email: this.state.account.email }
                    });
                    //if (res.Succeeded) {
                    //    // go to we sent you email page
                    //} else {
                    //    if (res.Code !== "DuplicateUserName") {
                    //        // go to Error Page
                    //    }
                    //    this.setState({
                    //        submitted: false,
                    //        response: res
                    //    });
                    //}
                });
        });
    }

    render() {
        const { classes } = this.props;
        const { account } = this.state;
        const { response } = this.state;
        const { submitted } = this.state;

        return (
            <div>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={3}
                >
                    <ValidatorForm
                        ref="form"
                        onSubmit={this.handleSubmit}
                    >
                        <Grid item xs={12}>
                            <Typography variant="h4">
                                Sign Up
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextValidator
                                fullWidth
                                error={!response.succeeded}
                                variant="outlined"
                                label="Email"
                                onChange={this.handleChange}
                                name="email"
                                value={account.email}
                                isValid={false}
                                validators={['required', 'isEmail']}
                                errorMessages={['this field is required', 'Email is not valid']}
                            />
                            <Typography hidden={response.succeeded} className={classes.error} color="error">
                                {response.message}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextValidator
                                fullWidth
                                type="password"
                                variant="outlined"
                                label="Password"
                                onChange={this.handleChange}
                                name="password"
                                value={account.password}
                                validators={['required', 'minStringLength:8','checkComplexPassword']}
                                errorMessages={['this field is required', 'At least 8 characters', 'Aa1!']}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextValidator
                                fullWidth
                                type="password"
                                variant="outlined"
                                label="Confirm Password"
                                onChange={this.handleChange}
                                name="confirmPassword"
                                value={account.confirmPassword}
                                validators={['required', 'isPasswordMatch']}
                                errorMessages={['this field is required', 'Password mismatch']}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <div className={classes.button}>
                                <Button
                                    color="primary"
                                    variant="contained"
                                    type="submit"
                                    disabled={submitted}
                                >
                                    {
                                        (submitted && 'Loading...')
                                        || (!submitted && 'Login')
                                    }
                                </Button>
                            </div>
                        </Grid>
                    </ValidatorForm>
                </Grid>
            </div>
        );
    }
}

SignUp.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignUp);