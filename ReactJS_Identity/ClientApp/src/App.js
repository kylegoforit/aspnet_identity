import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/home/Counter';
import Login from './components/account/Login';
import SignUp from './components/account/SignUp';
import ConfirmEmail from './components/account/ConfirmEmail';
import ReSendVerificationLink from './components/account/ReSendVerificationLink';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/home/counter' component={Counter} />
        <Route path='/fetch-data' component={FetchData} />
        <Route path='/account/login' component={Login} />
        <Route path='/account/sign-up' component={SignUp} />
        <Route path='/account/re-send-verification-link' component={ReSendVerificationLink} />
        <Route path='/account/confirm-email' component={ConfirmEmail} />
      </Layout>
    );
  }
}
