﻿using System;

namespace ReactJS_Identity.Models.API
{
    public abstract class BaseResponse
    {
        public abstract DateTime ResponseTime { get; set; }
        public abstract string ReturnUrl { get; set; }
        public abstract string Message { get; set; }
        public abstract string Code { get; set; }
        public abstract bool Succeeded { get; set; }
    }
}
