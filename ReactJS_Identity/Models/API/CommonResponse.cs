﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactJS_Identity.Models.API
{
    public class CommonResponse : BaseResponse
    {
        public override DateTime ResponseTime { get; set; }
        public override string Message { get; set; }
        public override string Code { get; set; }
        public override bool Succeeded { get; set; }
        public override string ReturnUrl { get; set; }
        public CommonResponse()
        {
            this.Succeeded = false;
            this.ResponseTime = DateTime.Now;
        }
    }
}
